========
For devs
========

.. toctree::
   :maxdepth: 2

   general
   tutorial
   plugin
   api/slidge/index
