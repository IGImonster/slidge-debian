Telegram
--------

Slidge uses the official telegram's library: `tdlib <https://tdlib.github.io/td/>`_.

You can customize tdlib's root dir and (local) encryption key with ``--tdlib-path``
and ``--tdlib-key``.
