===============
Plugin-specific
===============

Most plugins work without further configurations, but some require a little more fiddling.

.. toctree::
   signal
   telegram