Slidge
======

.. image:: https://badge.fury.io/py/slidge.svg
  :alt: PyPI package
  :target: https://pypi.org/project/slidge/

Slidge is a general purpose XMPP gateway framework in python.

Homepage: `sourcehut <https://sr.ht/~nicoco/slidge>`_

Chat room:
`slidge@conference.nicoco.fr <xmpp:slidge@conference.nicoco.fr?join>`_

Issue tracker: https://todo.sr.ht/~nicoco/slidge


.. toctree::
   :maxdepth: 2

   user/index
   admin/index
   dev/index
   glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
