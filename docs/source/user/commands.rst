Commands
========

Slidge plugins may provide additional commands besides "chat" or register.
You can discover them either by sending "help" to the slidge component's JID
or via adhoc commands (:xep:`0050`), if your XMPP client does that.