Registration
============

To make it work, you must "link" the foreign accounts you want to use with your
XMPP account by registering to the slidge server component.
The preferred way is to use in-band registration (:xep:`0077`)_, if your client supports it.
If your client does not, slidge has got you covered anyway!
Just send a chat message saying "register" to the component's JID, and follow the instructions.

.. figure:: gajim.png
   :scale: 50 %
   :alt: Gajim service discovery

   In gajim, go to the "accounts" menu and select "discover services".

.. figure:: movim1.png
   :scale: 50 %
   :alt: Movim gateway discovery

   In Movim, you can see the gateways in settings->account.

.. figure:: movim2.png
   :scale: 50 %
   :alt: Movim registration form

   An example registration in Movim.