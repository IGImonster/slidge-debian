Steam
-----

.. warning::
  I have no idea if steam is OK with you using slidge.

Roster
******

Contacts JIDs are of the form ``123459789@signal.example.com`` where ``123459789`` is a
steam user ID.

Your friends are added to your roster on registration.
