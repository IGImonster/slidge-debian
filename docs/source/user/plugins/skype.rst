Skype
-----

.. warning::
  To do things the right way, you should set up an app password that slidge will use.

Roster
******

Contacts JIDs are of the form ``username@slidge-skype.example.com`` where ``username`` is a
skype username.
Skype accounts linked to microsoft live accounts look like ``__live__somegibberish``.