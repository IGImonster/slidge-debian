Hacker news
-----------

.. note::
  This is just a quick and dirty way of getting hackernews comment reply notifications.
  There is no "chat" feature here.

Contact JIDs are of the form ``+123456789@signal.example.com`` where +123456789 is a thread ID.
You can reply to a reply by replying to the message.