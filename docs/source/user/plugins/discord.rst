Discord
-------

.. warning::
  Discord really dislikes alternative clients such as slidge (called "self bots" in their jargon),
  and you're breaking discord terms of use by using it. You might get your discord account suspended.
  Now you're warned. See :ref:`Keeping a low profile` for general tips about how to prevent this.

Roster
******

Contacts JIDs are of the form ``cooldude#4561@signal.example.com`` where ``cooldude`` is a
discord username and ``4561`` their discriminator.
