from pathlib import Path
from typing import Union

AvatarType = Union[bytes, str, Path]
LegacyUserIdType = Union[str, int]
LegacyMessageType = Union[str, int]
LegacyContactIdType = Union[str, int]
